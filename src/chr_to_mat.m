function mat = chr_to_mat(c)
% Convert an n x m char array to a regular numeric matrix

mat = zeros(length(c(:,1)), 1);
for i = 1:length(c(:,1))
	val = str2num(c(i, :))
	if isempty(val)
		val = NaN;
	end
	mat(i) = val;
end
