load('new_data/field_test_info.mat'); % fueltype1, house1, test_start, test_stop

load('new_data/field_toxin_values.mat'); % TSP_all_ug_m3, co_all_mgm3, co_time_start1, co_time_stop1

TSP_tests_to_skip = [4, 7, 8, 13, 16, 17];

CO_col = chr_to_mat(co_all_mgm3);
TSP_col = chr_to_mat(TSP_all_ug_m3);

CO_avgs = struct('wood', [], 'WC', [], 'AWC', []);
TSP_avgs = struct('wood', [], 'WC', [], 'AWC', []);
CO_box_data = []
TSP_box_data = []
grouping = []
colors = []
color_map = struct('wood', 'r', 'WC', 'g', 'AWC', 'b');
labels = {}

for i = 1:length(test_start)
	start = str2num(test_start(i, :))
	stop = str2num(test_stop(i, :))
	house = house1(i, :);
	fuel_num = deblank(fueltype1(i, :))
	CO_data = CO_col(start:stop);
	CO_box_data = horzcat(CO_box_data, CO_data');
	grouping = horzcat(grouping, i * ones(1, length(CO_data)));
	colors = horzcat(colors, color_map.(fuel_num));
	labels = horzcat(labels, house);
	CO_avg = sum(CO_data) / length(CO_data);
	CO_avgs.(fuel_num)(end + 1) = CO_avg;

	TSP_data = TSP_col(start:stop);
	TSP_avg = sum(TSP_data) / length(TSP_data);
	if any(TSP_tests_to_skip == i)
		TSP_box_data = horzcat(TSP_box_data, NaN(1, length(TSP_data)));
	else
		TSP_box_data = horzcat(TSP_box_data, TSP_data');
		TSP_avgs.(fuel_num)(end + 1) = TSP_avg;
	end
	length(CO_box_data)
	length(TSP_box_data)
	length(grouping)
end

CO_avg_data = [];
TSP_avg_data = [];
CO_avg_grouping = [];
TSP_avg_grouping = [];

for fuel = {'wood', 'WC', 'AWC'}
	i = i + 1;
	fuel = fuel{:};
	CO_avg_data = horzcat(CO_avg_data, CO_avgs.(fuel));
	TSP_avg_data = horzcat(TSP_avg_data, TSP_avgs.(fuel));
	CO_avg_grouping = horzcat(CO_avg_grouping, i * ones(1, length(CO_avgs.(fuel))));
	TSP_avg_grouping = horzcat(TSP_avg_grouping, i * ones(1, length(TSP_avgs.(fuel))));
	colors = horzcat(colors, color_map.(fuel));
	labels = horzcat(labels, 'avg');
end

figure('OuterPosition', [0, 0, 600, 900])
subplot 211
boxplot(horzcat(TSP_box_data, TSP_avg_data), horzcat(grouping, TSP_avg_grouping), 'colors', colors, 'labels', labels, 'symbol', '.')
ylim([-0.1e4, 1e4])
set(gca, 'FontSize', 24)
ylabel('TSP (\mu g/m^3)')

subplot 212
boxplot(horzcat(CO_box_data, CO_avg_data), horzcat(grouping, CO_avg_grouping), 'colors', colors, 'labels', labels, 'symbol', '.')
set(gca, 'FontSize', 24)
ylim([-10, 50])
ylabel('CO (mg/m^3)')
format_and_save('revised_graphs/field_compare.pdf')

figure('OuterPosition', [0, 0, 600, 900])
subplot 211
boxplot(TSP_avg_data, TSP_avg_grouping, 'colors', 'rgb', 'labels', {'Wood', 'WC', 'AWC'}, 'symbol', '.')
set(gca, 'FontSize', 24)
ylabel('TSP (\mu g/m^3)')

subplot 212
boxplot(CO_avg_data, CO_avg_grouping, 'colors', 'rgb', 'labels', {'Wood', 'WC', 'AWC'}, 'symbol', '.')
set(gca, 'FontSize', 24)
ylabel('CO (mg/m^3)')
format_and_save('revised_graphs/field_avg.pdf')



