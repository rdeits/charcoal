function plot_toxin_with_old_data(toxin)

load('results_data/new_toxin_totals.mat')
load('results_data/old_toxin_totals.mat')

%%%%%%%% Example plot combining old and new data

% Create a new figure
figure()

% Combine all the data (horzcat means concatenate the arrays horizontally) for the new data flaming and heating, plust the old data heating (since there's no flaming/heating distinction for those data)
toxin_WC_flaming_heating = horzcat(new_toxin_totals.WC.(toxin).flaming, new_toxin_totals.WC.(toxin).heating, old_toxin_totals.WC.(toxin).heating);
toxin_AWC_flaming_heating = horzcat(new_toxin_totals.AWC.(toxin).flaming, new_toxin_totals.AWC.(toxin).heating, old_toxin_totals.AWC.(toxin).heating);

% Get the boiling data for each fuel type
toxin_WC_boiling = horzcat(new_toxin_totals.WC.(toxin).boiling, old_toxin_totals.WC.(toxin).boiling);
toxin_AWC_boiling = horzcat(new_toxin_totals.AWC.(toxin).boiling, old_toxin_totals.AWC.(toxin).boiling);

% Get the entirety data
toxin_WC_entirety = horzcat(new_toxin_totals.WC.(toxin).entirety, old_toxin_totals.WC.(toxin).entirety);
toxin_AWC_entirety = horzcat(new_toxin_totals.AWC.(toxin).entirety, old_toxin_totals.AWC.(toxin).entirety);

% Don't clear the graph before each 'plot' command
hold on

% Draw the circles for the WC and AWC means
Y = [mean(toxin_WC_flaming_heating), mean(toxin_WC_boiling), mean(toxin_WC_entirety)];
E = [std(toxin_WC_flaming_heating), std(toxin_WC_boiling), std(toxin_WC_entirety)];
errorbar([1, 4, 7], Y, E, 'ro')

Y = [mean(toxin_AWC_flaming_heating), mean(toxin_AWC_boiling), mean(toxin_AWC_entirety)];
E = [std(toxin_AWC_flaming_heating), std(toxin_AWC_boiling), std(toxin_AWC_entirety)];
errorbar([2, 5, 8], Y, E, 'bo')

set(gca, 'xtick', [1.5, 4.5, 7.5])
set(gca, 'xTickLabel', {'Heating', 'Boiling', 'Entirety'})
legend('Wood Charcoal','Ag. Waste Charcoal')
ylabel('Average Integrated Emissions')
xlabel('Burn Phase')

print('-dpdf', strcat(toxin, '.pdf'))
