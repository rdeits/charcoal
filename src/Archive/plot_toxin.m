function plot_toxin_with_old_data(toxin)

load('results_data/new_toxin_totals.mat')

%%%%%%%% Example plot combining old and new data

% Create a new figure
figure()

toxin_WC_flaming = new_toxin_totals.WC.(toxin).flaming;
toxin_AWC_flaming = new_toxin_totals.AWC.(toxin).flaming;

toxin_WC_heating = new_toxin_totals.WC.(toxin).heating;
toxin_AWC_heating = new_toxin_totals.AWC.(toxin).heating;

toxin_WC_boiling = new_toxin_totals.WC.(toxin).boiling;
toxin_AWC_boiling = new_toxin_totals.AWC.(toxin).boiling;

toxin_WC_entirety = new_toxin_totals.WC.(toxin).entirety;
toxin_AWC_entirety = new_toxin_totals.AWC.(toxin).entirety;

% Don't clear the graph before each 'plot' command
hold on

% Draw the circles for the WC and AWC means
Y = [mean(toxin_WC_flaming), mean(toxin_WC_heating), mean(toxin_WC_boiling), mean(toxin_WC_entirety)];
E = [std(toxin_WC_flaming), std(toxin_WC_heating), std(toxin_WC_boiling), std(toxin_WC_entirety)];
errorbar([1, 4, 7, 10], Y, E, 'ro')

Y = [mean(toxin_AWC_flaming), mean(toxin_AWC_heating), mean(toxin_AWC_boiling), mean(toxin_AWC_entirety)];
E = [std(toxin_AWC_flaming), std(toxin_AWC_heating), std(toxin_AWC_boiling), std(toxin_AWC_entirety)];
errorbar([2, 5, 8, 11], Y, E, 'bo')

set(gca, 'xtick', [1.5, 4.5, 7.5, 10.5])
set(gca, 'xTickLabel', {'Flaming', 'Heating', 'Boiling', 'Entirety'})
legend('Wood Charcoal','Ag. Waste Charcoal')
ylabel('Average Integrated Emissions')
xlabel('Burn Phase')

print('-dpdf', strcat(toxin, '.pdf'))
