% Convert the thermocouple data into serial datenums

thermo_data = load('processed_data/thermo_data.mat');
runs = thermo_data.runs;

thermo_runs = struct('datetime', {}, 'temp_C', {});

for i = 1:length(runs)
	new_run = struct('datetime', datenum(runs{i}.dts(:,:), 'yyyy-mm-dd HH:MM:SS'), 'temp_C', runs{i}.temp_C);
	thermo_runs = [thermo_runs, new_run];
end

save('processed_data/thermo_runs.mat', 'thermo_runs')