% Similar to split_runs.m, but designed to extract the "old_data" from its entirely different filetype

start_ndxs = csvread('raw_data/old_data/bach_start.txt', 1);
stop_ndxs = csvread('raw_data/old_data/bach_end.txt', 1);
start_dts = read_date_file('processed_data/old_data_start_dts.txt', length(start_ndxs));

CO_all = csvread('raw_data/old_data/CO_all.txt', 1);
NO2_all = csvread('raw_data/old_data/NO2_all.txt', 1);
SO2_all = csvread('raw_data/old_data/SO2_all.txt', 1);
TSTACK_all = csvread('raw_data/old_data/TSTACK_all.txt', 1);

dt_all = read_date_file('processed_data/batch_time_all.txt', length(CO_all));

% dusttrak_all = csvread('raw_data/old_data/dusttrak_all.txt', 1);
% dusttrak_dt_all = read_date_file('processed_data/old_dusttrak_time_all.txt', length(dusttrak_all));
load('processed_data/old_dusttrak_all.mat');
load('processed_data/old_dusttrak_dt_all.mat');
dusttrak_stop_ndxs = find(dusttrak_dt_all(2:end) - (dusttrak_dt_all(1:end-1)) > 0.01);
dusttrak_start_ndxs = vertcat(1, dusttrak_stop_ndxs + 1);
dusttrak_stop_ndxs(end+1) = length(dusttrak_dt_all);

load('processed_data/thermo_runs.mat');

% Sort the thermocouple runs by their start time
thermo_start_dts = zeros(length(thermo_runs), 1);
for i=1:length(thermo_runs)
	thermo_start_dts(i) = thermo_runs(i).datetime(1);
end
[B, IX] = sort(thermo_start_dts);
thermo_runs = thermo_runs(IX);


fuel_type_awc = csvread('raw_data/old_data/ft_AWC.txt', 1);
fuel_type_wfwc = csvread('raw_data/old_data/ft_WFWC.txt', 1);

old_stove_results = struct('start_datetime', {}, 'fuel_type', {}, 'dusttrak_data', {}, 'toxin_data', {}, 'thermocouple_data', {});

for i = 1:length(start_ndxs)
	start_ndx = start_ndxs(i) + 1  % zero vs. one indexing
	stop_ndx = stop_ndxs(i) + 1

	toxin_data = struct('datetime', dt_all(start_ndx:stop_ndx), 'CO', CO_all(start_ndx:stop_ndx), 'NO2', NO2_all(start_ndx:stop_ndx), 'SO2', SO2_all(start_ndx:stop_ndx), 'tstack', TSTACK_all(start_ndx:stop_ndx));

	if fuel_type_awc(i) == 1
		fuel_type = 'AWC';
	elseif fuel_type_wfwc(i) == 1
		fuel_type = 'WC';
	else
		fuel_type = 'unknown';
	end

	datestr(dt_all(start_ndx))
	datestr(dt_all(stop_ndx))
	dusttrak_start_ndx = dusttrak_start_ndxs(i)
	dusttrak_stop_ndx = dusttrak_stop_ndxs(i)
	datestr(dusttrak_dt_all(dusttrak_start_ndx))
	datestr(dusttrak_dt_all(dusttrak_stop_ndx))
	dusttrak_data = struct('datetime', dusttrak_dt_all(dusttrak_start_ndx:dusttrak_stop_ndx), 'dusttrak_PM', dusttrak_all(dusttrak_start_ndx:dusttrak_stop_ndx));

	thermocouple_data = struct('datetime', thermo_runs(i).datetime, 'temp_C', thermo_runs(i).temp_C);

	result = struct('start_datetime', start_dts(i), 'fuel_type', fuel_type, 'dusttrak_data', dusttrak_data, 'toxin_data', toxin_data, 'thermocouple_data', thermocouple_data);
	old_stove_results = [old_stove_results, result];
end

save('results_data/old_stove_results.mat', 'old_stove_results');

