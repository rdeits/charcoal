function datenums = read_date_file(fname, num_rows)

fid = fopen(fname, 'r');
datenums = zeros(num_rows, 1);
for i = 1:num_rows
	datenums(i) = datenum(fgetl(fid), 'yyyy-mm-dd HH:MM:SS');
end
fclose(fid);