"""
Convert dates from seconds since 1/1/1904 to ISO format datestrings

usage: 
python convert_dates.py <source_file> <destination_file>
"""

import sys
import csv
import datetime


input_fname = sys.argv[1]
output_fname = sys.argv[2]

r = csv.reader(open(input_fname, 'rU'))
start_times_raw = [line for line in r]
start_times_raw = reduce(list.__add__, start_times_raw)
start_times_raw = start_times_raw[1:]
start_deltas = [datetime.timedelta(seconds=int(x)) for x in start_times_raw]
start_dts = [datetime.datetime(1904, 1, 1) + td for td in start_deltas]
start_datestrings = [dt.isoformat(' ') for dt in start_dts]
w = csv.writer(open(output_fname, 'w'))
w.writerows([[x] for x in start_datestrings])
