import os
import datetime
import scipy.io
import numpy as np
import csv

data_folder = 'raw_data/old_data'

files = (f for f in os.listdir(data_folder) if 'thermo' in f)

runs = []

for fname in files:
    print fname
    r = csv.reader(open(os.path.join(data_folder, fname), 'rU'))
    r.next()  # skip the first row
    raw_data = [line for line in r]
    if len(raw_data[0]) == 1:
        # One of the thermo files has tab-separated values, instead of comma separated, and its time fields are all screwed up too
        print "detected non-comma-separated file"
        r = csv.reader(open(os.path.join(data_folder, fname), 'rU'), delimiter='\t')
        r.next()  # skip the first row
        raw_data = [line for line in r]
        dts = np.array([datetime.datetime.strptime(line[1], '%m/%d/%y %H:%M').isoformat(' ') for line in raw_data])
    else:
        dts = np.array([datetime.datetime.strptime(line[1], '%d/%m/%Y %H:%M:%S').isoformat(' ') for line in raw_data])
    temp_C = np.array([float(line[2]) for line in raw_data])
    runs.append({'dts': dts, 'temp_C': temp_C})

scipy.io.savemat('processed_data/thermo_data.mat', {'runs': runs})
