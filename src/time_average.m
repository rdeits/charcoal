function time_avg = time_average(x, y)

% Calculate the time average of y, assuming x is given in Matlab datenum format (with units of days)

time_avg = trapz(x, y) / ((max(x) - min(x)));