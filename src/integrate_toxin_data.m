load('results_data/stove_results.mat')
load('results_data/old_stove_results.mat')

% Integrate the toxin data to determine the average emissions per second for each of the listed toxins for each fuel type. 

new_data_toxins = {'CO2', 'PAH', 'BC', 'OC', 'smps_V', 'dusttrak_PM', 'CO'};

old_data_toxins = {'dusttrak_PM', 'CO'};

old_toxin_totals = struct();
new_toxin_totals = struct();

for fuel_type = {'WC', 'AWC'}
	for toxin = old_data_toxins
		toxin_heating_totals = [];
		toxin_boiling_totals = [];
		toxin_entirety_totals = [];
		for i = 1:length(old_stove_results)
			test = old_stove_results(i);
			if ~strcmp(test.fuel_type, fuel_type{1})
				continue
			end
			boiling_start_ndx = find(test.thermocouple_data.temp_C >= 100, 1, 'first');
			if isempty(boiling_start_ndx)
				boiling_start_ndx = length(test.thermocouple_data.temp_C);
			end
			boiling_start_dt = test.thermocouple_data.datetime(boiling_start_ndx);

			dusttrak_boiling_start_ndx = find(test.dusttrak_data.datetime >= boiling_start_dt, 1, 'first');

			toxin_boiling_start_ndx = find(test.toxin_data.datetime >= boiling_start_dt, 1, 'first');

			if strcmp(toxin, 'dusttrak_PM')
				if dusttrak_boiling_start_ndx <= 2
					toxin_heating_total = NaN;
				else
					x = test.dusttrak_data.datetime(1:dusttrak_boiling_start_ndx);
					y = test.dusttrak_data.dusttrak_PM(1:dusttrak_boiling_start_ndx);
					toxin_heating_total = time_average(x, y);
				end
				if dusttrak_boiling_start_ndx >= (length(test.dusttrak_data.datetime) - 1)
					toxin_boiling_total = NaN;
				else
					x = test.dusttrak_data.datetime(dusttrak_boiling_start_ndx:end);
					y = test.dusttrak_data.dusttrak_PM(dusttrak_boiling_start_ndx:end);
					toxin_boiling_total = time_average(x, y);
				end
				x = test.dusttrak_data.datetime;
				y = test.dusttrak_data.dusttrak_PM;
				toxin_entirety_total = time_average(x, y);
			else
				if toxin_boiling_start_ndx <= 2
					toxin_heating_total = NaN;
				else
					x = test.toxin_data.datetime(1:toxin_boiling_start_ndx);
					y = test.toxin_data.(toxin{1})(1:toxin_boiling_start_ndx);
					toxin_heating_total = time_average(x, y);
				end
				if toxin_boiling_start_ndx >= (length(test.toxin_data.datetime) - 1)
					toxin_boiling_total = NaN;
				else
					x = test.toxin_data.datetime(toxin_boiling_start_ndx:end);
					y = test.toxin_data.(toxin{1})(toxin_boiling_start_ndx:end);
					toxin_boiling_total = time_average(x, y);
				end
				x = test.toxin_data.datetime;
				y = test.toxin_data.(toxin{1});
				toxin_entirety_total = time_average(x, y);
			end
			if ~isnan(toxin_heating_total)
				toxin_heating_totals(end+1) = toxin_heating_total;
			end
			if ~isnan(toxin_boiling_total)
				toxin_boiling_totals(end+1) = toxin_boiling_total;
			end
			if ~isnan(toxin_entirety_total)
				toxin_entirety_totals(end+1) = toxin_entirety_total;
			end
		end
		old_toxin_totals.(fuel_type{1}).(toxin{1}).heating =toxin_heating_totals;
		old_toxin_totals.(fuel_type{1}).(toxin{1}).boiling = toxin_boiling_totals;
		old_toxin_totals.(fuel_type{1}).(toxin{1}).entirety = toxin_entirety_totals;
	end

	for toxin = new_data_toxins
		toxin_flaming_totals = [];
		toxin_heating_totals = [];
		toxin_boiling_totals = [];
		toxin_entirety_totals = [];
		for i = 1:length(stove_results)
			test = stove_results(i);
			if ~strcmp(test.fuel_type, fuel_type{1})
				continue
			end

			if test.lighter_stop_ndx <= 2
				toxin_flaming_total = NaN;
			else
				x = test.toxin_data.datetime(1:test.lighter_stop_ndx);
				y = test.toxin_data.(toxin{1})(1:test.lighter_stop_ndx);
				toxin_flaming_total = time_average(x, y);
			end

			if test.water_boil_ndx - test.lighter_stop_ndx >= 2
				x = test.toxin_data.datetime(test.lighter_stop_ndx:test.water_boil_ndx);
				y = test.toxin_data.(toxin{1})(test.lighter_stop_ndx:test.water_boil_ndx);
				toxin_heating_total = time_average(x, y);
			else
				toxin_heating_total = NaN;
			end

			if test.water_boil_ndx < (length(test.toxin_data.datetime) - 1)
				x = test.toxin_data.datetime(test.water_boil_ndx:end);
				y =  test.toxin_data.(toxin{1})(test.water_boil_ndx:end);
				toxin_boiling_total = time_average(x, y);
			else
				toxin_boiling_total = NaN;
			end
			x = test.toxin_data.datetime;
			y = test.toxin_data.(toxin{1});
			toxin_entirety_total = time_average(x, y);

			if ~isnan(toxin_flaming_total)
				toxin_flaming_totals(end+1) = toxin_flaming_total;
			end
			if ~isnan(toxin_heating_total)
				toxin_heating_totals(end+1) = toxin_heating_total;
			end
			if ~isnan(toxin_boiling_total)
				toxin_boiling_totals(end+1) = toxin_boiling_total;
			end
			if ~isnan(toxin_entirety_total)
				toxin_entirety_totals(end+1) = toxin_entirety_total;
			end
		end
		new_toxin_totals.(fuel_type{1}).(toxin{1}).flaming = toxin_flaming_totals;
		new_toxin_totals.(fuel_type{1}).(toxin{1}).heating = toxin_heating_totals;
		new_toxin_totals.(fuel_type{1}).(toxin{1}).boiling = toxin_boiling_totals;
		new_toxin_totals.(fuel_type{1}).(toxin{1}).entirety = toxin_entirety_totals;
	end
end

save('results_data/old_toxin_totals.mat', 'old_toxin_totals')
save('results_data/new_toxin_totals.mat', 'new_toxin_totals')
