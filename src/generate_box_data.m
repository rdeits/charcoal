function [box_data, grouping] = generate_box_data(data, starts, stops, types, scaling_factors)
% Generate boxplot data from a column vector and a column of start and stop indices. 

totals = struct();
all_types = fieldnames(scaling_factors)'
scaling_factors
for t = all_types
	totals.(t{:}) = [];
end

for i = 1:length(starts)
	start = starts(i);
	stop = stops(i);
	type = deblank(types(i,:));
	if any(strcmp(fieldnames(totals), type))
		values = data(start:stop);
		avg = sum(values) / length(values);
		avg = avg * scaling_factors.(type);
		totals.(type)(end + 1) = avg;
	end
end

grouping = [];
box_data = [];
i = 1;
for t = all_types
	grouping = horzcat(grouping, i * ones(1, length(totals.(t{:}))));
	i = i + 1;
	box_data = horzcat(box_data, totals.(t{:}));
end

