load('new_data/AWC_WC_PWB_test_info.mat') % creates fuel_name2, t_lighter_stop_pnt2, t_start_time_pnt2, t_water_boiling_pnt2, t_test_concluded_pnt2

load('new_data/AWC_WC_PWB_toxin_values.mat') % creates PAHavg_Vavg_d, COavg_Vavg_mgm3, TSP_ugm3, smps_integrated_N_d, smps_integrated_V_d, smps_mode_dia1002_d
AWC_TSP_ugm3 = TSP_ugm3;

load('new_data/waste_test_info.mat') % fire_start1, fire_stop1, labels_waste_only

load('new_data/waste_toxin_values.mat') % Bach_Time_start, Bach_Time_stop, CO_mgm3, TSP_ugm3

load('new_data/AWC_energy_densities.mat') % MJ_kg1, labels_awcopt1
scaling_factors = struct();
MJ_kg = chr_to_mat(MJ_kg1);
for i = 1:length(labels_awcopt1(:,1))
	[fuel, ~] = strtok(labels_awcopt1(i, :), ',');
	scaling_factors.(fuel) = 1 / MJ_kg(i);
end

load('new_data/waste_energy_densities.mat') % Mj_kg, label_Waste
MJ_kg = chr_to_mat(Mj_kg);
for i = 1:length(label_Waste(:,1))
	[fuel, ~] = strtok(label_Waste(i, :), ',');
	scaling_factors.(fuel) = 1 / MJ_kg(i);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TSP/MJ
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[boxplot_data1, grouping1] = generate_box_data(chr_to_mat(AWC_TSP_ugm3), chr_to_mat(t_start_time_pnt2), chr_to_mat(t_test_concluded_pnt2), fuel_name2, struct('AWC', scaling_factors.AWC, 'WC', scaling_factors.WC));

[boxplot_data2, grouping2] = generate_box_data(chr_to_mat(TSP_ugm3), chr_to_mat(fire_start1), chr_to_mat(fire_stop1), labels_waste_only, struct('FWB', scaling_factors.FWB, 'FWC', scaling_factors.FWC, 'PWB', scaling_factors.PWB, 'PWC', scaling_factors.PWC));

grouping2 = grouping2 + max(grouping1) + 1;
boxplot_data = horzcat(boxplot_data1, boxplot_data2);
grouping = horzcat(grouping1, grouping2);

figure
boxplot(boxplot_data, grouping, 'colors', 'b', 'labels', {'AWC, n=6', 'WC, n=6', 'FWB, n=2', 'FWC, n=3', 'PWB, n=2', 'PWC, n=2'}, 'symbol', '.')
set(gca, 'FontSize', 14)
ylabel('TSP/Energy Density ((\mu g/m^3)/(MJ/kg))')

format_and_save('revised_graphs/waste_TSP_MJ.pdf')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TSP
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[boxplot_data1, grouping1] = generate_box_data(chr_to_mat(AWC_TSP_ugm3), chr_to_mat(t_start_time_pnt2), chr_to_mat(t_test_concluded_pnt2), fuel_name2, struct('AWC', 1, 'WC', 1));

[boxplot_data2, grouping2] = generate_box_data(chr_to_mat(TSP_ugm3), chr_to_mat(fire_start1), chr_to_mat(fire_stop1), labels_waste_only, struct('FWB', 1, 'FWC', 1, 'PWB', 1, 'PWC', 1));

grouping2 = grouping2 + max(grouping1) + 1;
boxplot_data = horzcat(boxplot_data1, boxplot_data2);
grouping = horzcat(grouping1, grouping2);

figure
boxplot(boxplot_data, grouping, 'colors', 'b', 'labels', {'AWC, n=6', 'WC, n=6', 'FWB, n=2', 'FWC, n=3', 'PWB, n=2', 'PWC, n=2'}, 'symbol', '.')
set(gca, 'FontSize', 14)
ylabel('TSP (\mu g/m^3)')

format_and_save('revised_graphs/waste_TSP.pdf')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% CO/MJ
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[boxplot_data1, grouping1] = generate_box_data(chr_to_mat(COavg_Vavg_mgm3), chr_to_mat(t_start_time_pnt2), chr_to_mat(t_test_concluded_pnt2), fuel_name2, struct('AWC', scaling_factors.AWC, 'WC', scaling_factors.WC));

[boxplot_data2, grouping2] = generate_box_data(chr_to_mat(CO_mgm3), chr_to_mat(fire_start1), chr_to_mat(fire_stop1), labels_waste_only, struct('FWB', scaling_factors.FWB, 'FWC', scaling_factors.FWC, 'PWB', scaling_factors.PWB, 'PWC', scaling_factors.PWC));

grouping2 = grouping2 + max(grouping1) + 1;
boxplot_data = horzcat(boxplot_data1, boxplot_data2);
grouping = horzcat(grouping1, grouping2);

figure
boxplot(boxplot_data, grouping, 'colors', 'b', 'labels', {'AWC, n=6', 'WC, n=6', 'FWB, n=2', 'FWC, n=3', 'PWB, n=2', 'PWC, n=2'}, 'symbol', '.')
set(gca, 'FontSize', 14)
ylabel('Carbon Monoxide/Energy Density ((mg/m^3)/(MJ/kg))')

format_and_save('revised_graphs/waste_CO_MJ.pdf')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% CO
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[boxplot_data1, grouping1] = generate_box_data(chr_to_mat(COavg_Vavg_mgm3), chr_to_mat(t_start_time_pnt2), chr_to_mat(t_test_concluded_pnt2), fuel_name2, struct('AWC', 1, 'WC', 1));

[boxplot_data2, grouping2] = generate_box_data(chr_to_mat(CO_mgm3), chr_to_mat(fire_start1), chr_to_mat(fire_stop1), labels_waste_only, struct('FWB', 1, 'FWC', 1, 'PWB', 1, 'PWC', 1));

grouping2 = grouping2 + max(grouping1) + 1;
boxplot_data = horzcat(boxplot_data1, boxplot_data2);
grouping = horzcat(grouping1, grouping2);

figure
boxplot(boxplot_data, grouping, 'colors', 'b', 'labels', {'AWC, n=6', 'WC, n=6', 'FWB, n=2', 'FWC, n=3', 'PWB, n=2', 'PWC, n=2'}, 'symbol', '.')
set(gca, 'FontSize', 14)
ylabel('Carbon Monoxide (mg/m^3)')

format_and_save('revised_graphs/waste_CO.pdf')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% MJ/kg
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
box_data = [scaling_factors.AWC, scaling_factors.WC, scaling_factors.FWB, scaling_factors.FWC, scaling_factors.PWB, scaling_factors.PWC];
grouping = 1:length(box_data);
figure
boxplot(box_data, grouping, 'colors', 'b', 'labels', {'AWC, n=6', 'WC, n=6', 'FWB, n=2', 'FWC, n=3', 'PWB, n=2', 'PWC, n=2'}, 'symbol', '.')
set(gca, 'FontSize', 14)
ylabel('Energy Density (MJ/kg)')

format_and_save('revised_graphs/waste_MJ.pdf')
