# Load in the data for all the different tests and create organized .mat files for each run

import csv
import scipy.io
import numpy as np

FOLDER = 'new_data/'

for fname_base in ['AWC_energy_densities',
              'AWC_opt_test_info',
              'AWC_opt_toxin_values',
              'AWC_WC_PWB_test_info',
              'AWC_WC_PWB_toxin_values',
              'field_test_info',
              'field_toxin_values',
              'waste_energy_densities',
              'waste_test_info',
              'waste_toxin_values']:
    with open(FOLDER + fname_base + '.csv', 'rU') as csvfile:
        r = csv.DictReader(csvfile)
        data = dict.fromkeys(r.fieldnames)
        for key in data:
            data[key] = []
        for row in r:
            for key, val in row.items():
                data[key].append(val)


    scipy.io.savemat(FOLDER + fname_base + '.mat', data)
