load('new_data/AWC_WC_PWB_test_info.mat') % creates fuel_name2, t_lighter_stop_pnt2, t_start_time_pnt2, t_water_boiling_pnt2, t_test_concluded_pnt2

load('new_data/AWC_WC_PWB_toxin_values.mat') % creates PAHavg_Vavg_d, COavg_Vavg_mgm3, TSP_ugm3, smps_integrated_N_d, smps_integrated_V_d, smps_mode_dia1002_d

[boxplot_data, grouping] = generate_box_data(chr_to_mat(PAHavg_Vavg_d), chr_to_mat(t_start_time_pnt2), chr_to_mat(t_test_concluded_pnt2), fuel_name2, struct('WC', 1, 'AWC', 1, 'PWB', 1));

figure
lala=boxplot(boxplot_data, grouping, 'colors', 'bbg', 'labels', {'WC, n=6', 'AWC, n=6', 'PWB, n=1'}, 'symbol', '.');
set(gca, 'FontSize', 24)
ylabel('PAH (\mu g/m^3)')

format_and_save('revised_graphs/pah_avg.pdf')
