function format_and_save(fname)

% Set font size for y ticks
set(gca, 'FontSize', 24)

% Set axis line width
set(gca, 'LineWidth', 2)

% Set box line width
set(findobj(gcf, 'Tag', 'Box'), 'LineWidth', 2)

% Set median line width
set(findobj(gcf, 'Tag', 'Median'), 'LineWidth', 2)


set(gcf, 'PaperPositionMode', 'auto')
set(findobj(gcf,'Tag','Outliers'),'MarkerSize',5);
print('-dpdf', fname)