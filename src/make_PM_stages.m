load('new_data/AWC_WC_PWB_test_info.mat') % creates fuel_name2, t_lighter_stop_pnt2, t_start_time_pnt2, t_water_boiling_pnt2, t_test_concluded_pnt2

load('new_data/AWC_WC_PWB_toxin_values.mat') % creates PAHavg_Vavg_d, COavg_Vavg_mgm3, TSP_ugm3, smps_integrated_N_d, smps_integrated_V_d, smps_mode_dia1002_d

smps_N = chr_to_mat(smps_integrated_N_d);
smps_V = chr_to_mat(smps_integrated_V_d);
smps_dia = chr_to_mat(smps_mode_dia1002_d);
tsp = chr_to_mat(TSP_ugm3);

color_map = struct('WC', 'g', 'AWC', 'b');

tsp_flaming_avgs = struct('WC', [], 'AWC', []);
tsp_heating_avgs = struct('WC', [], 'AWC', []);
tsp_boiling_avgs = struct('WC', [], 'AWC', []);
tsp_entirety_avgs = struct('WC', [], 'AWC', []);
smps_N_flaming_avgs = struct('WC', [], 'AWC', []);
smps_N_heating_avgs = struct('WC', [], 'AWC', []);
smps_N_boiling_avgs = struct('WC', [], 'AWC', []);
smps_N_entirety_avgs = struct('WC', [], 'AWC', []);
smps_V_flaming_avgs = struct('WC', [], 'AWC', []);
smps_V_heating_avgs = struct('WC', [], 'AWC', []);
smps_V_boiling_avgs = struct('WC', [], 'AWC', []);
smps_V_entirety_avgs = struct('WC', [], 'AWC', []);
smps_dia_flaming_avgs = struct('WC', [], 'AWC', []);
smps_dia_heating_avgs = struct('WC', [], 'AWC', []);
smps_dia_boiling_avgs = struct('WC', [], 'AWC', []);
smps_dia_entirety_avgs = struct('WC', [], 'AWC', []);

for i = 1:length(t_start_time_pnt2)
	fuel = deblank(fuel_name2(i, :));
	if strcmp(fuel, 'PWB')
		continue
	end
	t_start = str2num(t_start_time_pnt2(i, :));
	t_heat_start = str2num(t_lighter_stop_pnt2(i, :));
	t_boil = str2num(t_water_boiling_pnt2(i, :));
	t_stop = str2num(t_test_concluded_pnt2(i, :));
	flaming = t_start:t_heat_start;
	heating = t_heat_start:t_boil;
	boiling = t_boil:t_stop;
	entirety = t_start:t_stop;
	tsp_flaming_avgs.(fuel)(end + 1) = sum(tsp(flaming)) / length(tsp(flaming));
	tsp_heating_avgs.(fuel)(end + 1) = sum(tsp(heating)) / length(tsp(heating));
	tsp_boiling_avgs.(fuel)(end + 1) = sum(tsp(boiling)) / length(tsp(boiling));
	tsp_entirety_avgs.(fuel)(end + 1) = sum(tsp(entirety)) / length(tsp(entirety));
	smps_N_flaming_avgs.(fuel)(end + 1) = sum(smps_N(flaming)) / length(smps_N(flaming));
	smps_N_heating_avgs.(fuel)(end + 1) = sum(smps_N(heating)) / length(smps_N(heating));
	smps_N_boiling_avgs.(fuel)(end + 1) = sum(smps_N(boiling)) / length(smps_N(boiling));
	smps_N_entirety_avgs.(fuel)(end + 1) = sum(smps_N(entirety)) / length(smps_N(entirety));
	smps_V_flaming_avgs.(fuel)(end + 1) = sum(smps_V(flaming)) / length(smps_V(flaming));
	smps_V_heating_avgs.(fuel)(end + 1) = sum(smps_V(heating)) / length(smps_V(heating));
	smps_V_boiling_avgs.(fuel)(end + 1) = sum(smps_V(boiling)) / length(smps_V(boiling));
	smps_V_entirety_avgs.(fuel)(end + 1) = sum(smps_V(entirety)) / length(smps_V(entirety));
	smps_dia_flaming_avgs.(fuel)(end + 1) = sum(smps_dia(flaming)) / length(smps_dia(flaming));
	smps_dia_heating_avgs.(fuel)(end + 1) = sum(smps_dia(heating)) / length(smps_dia(heating));
	smps_dia_boiling_avgs.(fuel)(end + 1) = sum(smps_dia(boiling)) / length(smps_dia(boiling));
	smps_dia_entirety_avgs.(fuel)(end + 1) = sum(smps_dia(entirety)) / length(smps_dia(entirety));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TSP and ultra-fine V
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

box_data = [];
grouping = [];
colors = []

[box_data, grouping, colors] = update_box_data(box_data, grouping, colors, tsp_flaming_avgs, color_map);
[box_data, grouping, colors] = update_box_data(box_data, grouping, colors, tsp_heating_avgs, color_map);
[box_data, grouping, colors] = update_box_data(box_data, grouping, colors, tsp_boiling_avgs, color_map);
[box_data, grouping, colors] = update_box_data(box_data, grouping, colors, tsp_entirety_avgs, color_map);
[box_data, grouping, colors] = update_box_data(box_data, grouping, colors, smps_V_flaming_avgs, color_map);
[box_data, grouping, colors] = update_box_data(box_data, grouping, colors, smps_V_heating_avgs, color_map);
[box_data, grouping, colors] = update_box_data(box_data, grouping, colors, smps_V_boiling_avgs, color_map);
[box_data, grouping, colors] = update_box_data(box_data, grouping, colors, smps_V_entirety_avgs, color_map);



figure('OuterPosition', [0, 0, 900, 400])
orient landscape
boxplot(box_data, grouping, 'colors', colors, 'symbol', '.')
set(gca, 'XTick', [1.5:2:15.5])
set(gca, 'XtickLabel', {'flaming', 'heating', 'boiling', 'entirety'})
set(gca, 'FontSize', 24)
ylabel('PM Emissions (\mu g/m^3)')
text(4.5, -1.25e4, 'TSP', 'FontSize', 24)
text(12.5, -1.25e4, 'ultra-fine', 'FontSize', 24)
format_and_save('revised_graphs/PM_stages_V.pdf')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ultra-fine N
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

box_data = [];
grouping = [];
colors = []

[box_data, grouping, colors] = update_box_data(box_data, grouping, colors, smps_N_flaming_avgs, color_map);
[box_data, grouping, colors] = update_box_data(box_data, grouping, colors, smps_N_heating_avgs, color_map);
[box_data, grouping, colors] = update_box_data(box_data, grouping, colors, smps_N_boiling_avgs, color_map);
[box_data, grouping, colors] = update_box_data(box_data, grouping, colors, smps_N_entirety_avgs, color_map);

% figure('OuterPosition', [0, 0, 900, 400])
figure
% orient landscape
boxplot(box_data, grouping, 'colors', colors, 'symbol', '.')
set(gca, 'XTick', [1.5:2:15.5])
set(gca, 'XtickLabel', {'flaming', 'heating', 'boiling', 'entirety'})
set(gca, 'FontSize', 24)
ylabel('PM Emissions (N/cm^3)')
% text(4.5, -1.25e4, 'TSP', 'FontSize', 24)
% text(12.5, -1.25e4, 'ultra-fine', 'FontSize', 24)
format_and_save('revised_graphs/PM_stages_N.pdf')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ultra-fine N
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

box_data = [];
grouping = [];
colors = []

[box_data, grouping, colors] = update_box_data(box_data, grouping, colors, smps_dia_flaming_avgs, color_map);
[box_data, grouping, colors] = update_box_data(box_data, grouping, colors, smps_dia_heating_avgs, color_map);
[box_data, grouping, colors] = update_box_data(box_data, grouping, colors, smps_dia_boiling_avgs, color_map);
[box_data, grouping, colors] = update_box_data(box_data, grouping, colors, smps_dia_entirety_avgs, color_map);

% figure('OuterPosition', [0, 0, 900, 400])
figure
% orient landscape
boxplot(box_data, grouping, 'colors', colors, 'symbol', '.')
set(gca, 'XTick', [1.5:2:15.5])
set(gca, 'XtickLabel', {'flaming', 'heating', 'boiling', 'entirety'})
set(gca, 'FontSize', 24)
ylabel('PM Emissions Mode Diameter (mm)')
% text(4.5, -1.25e4, 'TSP', 'FontSize', 24)
% text(12.5, -1.25e4, 'ultra-fine', 'FontSize', 24)
format_and_save('revised_graphs/PM_stages_dia.pdf')
