load('new_data/AWC_opt_test_info.mat') % fire_start, fire_stop, fueltype, fueltype1

load('new_data/AWC_opt_toxin_values.mat') % Bach_Time_start, Bach_Time_stop, CO_mgm3, TSP_ugm3

load('new_data/AWC_energy_densities.mat') % MJ_kg1, labels_awcopt1
scaling_factors = struct();
MJ_kg1
MJ_kg = chr_to_mat(MJ_kg1);
for i = 1:length(labels_awcopt1(:,1))
	[fuel, ~] = strtok(labels_awcopt1(i, :), ',')
	scaling_factors.(fuel) = 1 / MJ_kg(i);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% CO
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[boxplot_data, grouping] = generate_box_data(chr_to_mat(CO_mgm3), chr_to_mat(fire_start), chr_to_mat(fire_stop), fueltype1, struct('AWC', 1, 'AWCB', 1, 'AWCX', 1, 'AWCM', 1));

figure
boxplot(boxplot_data, grouping, 'colors', 'b', 'labels', {'AWC, n=3', 'AWCB, n=3', 'AWCX, n=3', 'AWCM, n=2'}, 'symbol', '.')
set(gca,'FontSize',14)
ylabel('Carbon Monoxide (mg/m^3)')

format_and_save('revised_graphs/AWC_opt_CO.pdf')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% CO per MJ/kg
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[boxplot_data, grouping] = generate_box_data(chr_to_mat(CO_mgm3), chr_to_mat(fire_start), chr_to_mat(fire_stop), fueltype1, scaling_factors);

figure
boxplot(boxplot_data, grouping, 'colors', 'b', 'labels', {'AWC, n=3', 'AWCB, n=3', 'AWCX, n=3', 'AWCM, n=2'}, 'symbol', '.')
set(gca,'FontSize',14)
ylabel('Carbon Monoxide/Energy Density ((mg/m^3)/(MJ/kg))')

format_and_save('revised_graphs/AWC_opt_CO_MJ.pdf')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TSP
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[boxplot_data, grouping] = generate_box_data(chr_to_mat(TSP_ugm3), chr_to_mat(fire_start), chr_to_mat(fire_stop), fueltype1, struct('AWC', 1, 'AWCB', 1, 'AWCX', 1, 'AWCM', 1));

figure
boxplot(boxplot_data, grouping, 'colors', 'b', 'labels', {'AWC, n=3', 'AWCB, n=3', 'AWCX, n=3', 'AWCM, n=2'}, 'symbol', '.')
set(gca, 'FontSize', 14)
ylabel('TSP (\mu g/m^3)')

format_and_save('revised_graphs/AWC_opt_TSP.pdf')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TSP per MJ/kg
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[boxplot_data, grouping] = generate_box_data(chr_to_mat(TSP_ugm3), chr_to_mat(fire_start), chr_to_mat(fire_stop), fueltype1, scaling_factors);

figure
boxplot(boxplot_data, grouping, 'colors', 'b', 'labels', {'AWC, n=3', 'AWCB, n=3', 'AWCX, n=3', 'AWCM, n=2'}, 'symbol', '.')
set(gca, 'FontSize', 14)
ylabel('TSP/Energy Density ((\mu g/m^3)/(MJ/kg))')

format_and_save('revised_graphs/AWC_opt_TSP_MJ.pdf')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% MJ/kg
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
box_data = MJ_kg;
grouping = 1:length(box_data);
figure
boxplot(box_data, grouping, 'colors', 'b', 'labels', {'AWC, n=3', 'AWCB, n=3', 'AWCX, n=3', 'AWCM, n=2'}, 'symbol', '.')
set(gca, 'FontSize', 14)
ylabel('Energy Density (MJ/kg)')

format_and_save('revised_graphs/AWC_opt_MJ.pdf')


