stovematrix = load('raw_data/stovematrix.mat');
stovematrix = stovematrix.data;

% Load the (new) stove data, with the toxin data from stovematrix.mat and the various indices and annotations from the other text files, and generate a single .mat file called stove_results.mat with each run's data stored separately.

start_dt_fid = fopen('processed_data/start_datetimes.txt', 'r');
toxin_dt_fid = fopen('processed_data/stovematrix_date_time.txt', 'r');

test_start_ndxs = csvread('raw_data/t_start_time_pnt.txt', 1);
test_stop_ndxs = csvread('raw_data/t_test_concluded_pnt.txt', 1);

water_boil_ndxs = csvread('raw_data/t_water_boiling_pnt.txt', 1);
lighter_stop_ndxs = csvread('raw_data/t_lighter_stop_pnt.txt', 1);

fuel_consumed_g = csvread('raw_data/g_fuel_consumed.txt', 1);

fuel_type_awc = csvread('raw_data/ft_awc.txt',1);
fuel_type_wfwc = csvread('raw_data/ft_wfwc.txt', 1);

stove_results = struct('start_datetime', {}, 'water_boil_ndx', {}, 'lighter_stop_ndx', {}, 'fuel_consumed_g', {}, 'toxin_data',{}, 'fuel_type', {});

toxin_dts = zeros(length(stovematrix(:,1)));
for i = 1:length(stovematrix(:,1))
	line = fgetl(toxin_dt_fid);
	toxin_dts(i) = datenum(line, 'yyyy-mm-dd HH:MM:SS');
end

for i = 1:length(test_start_ndxs)
	start_ndx = test_start_ndxs(i) + 1;  % start points are 0 indexed
	stop_ndx = test_stop_ndxs(i) + 1;
	toxin_data = struct('datetime', toxin_dts(start_ndx:stop_ndx)', 'smps_mode_diameter', stovematrix(start_ndx:stop_ndx,1), 'smps_N', stovematrix(start_ndx:stop_ndx,2), 'smps_V', stovematrix(start_ndx:stop_ndx,3), 'dusttrak_PM', stovematrix(start_ndx:stop_ndx,4), 'CO', stovematrix(start_ndx:stop_ndx,5), 'NO2', stovematrix(start_ndx:stop_ndx,6), 'SO2', stovematrix(start_ndx:stop_ndx,7), 'CO2', stovematrix(start_ndx:stop_ndx,8), 'mz24', stovematrix(start_ndx:stop_ndx,9), 'mz57', stovematrix(start_ndx:stop_ndx,10), 'mz60', stovematrix(start_ndx:stop_ndx,11), 'mz202', stovematrix(start_ndx:stop_ndx,12), 'tpot', stovematrix(start_ndx:stop_ndx,13), 'tstack', stovematrix(start_ndx:stop_ndx,14), 'PAH', stovematrix(start_ndx:stop_ndx,15), 'BC', stovematrix(start_ndx:stop_ndx,16), 'OC', stovematrix(start_ndx:stop_ndx,17));

	if fuel_type_awc(i) == 1
		fuel_type = 'AWC';
	elseif fuel_type_wfwc(i) == 1
		fuel_type = 'WC';
	else
		fuel_type = 'unknown';
	end

	if water_boil_ndxs(i) >= test_start_ndxs(i)
		water_boil_ndx = water_boil_ndxs(i)-test_start_ndxs(i)+1;
	elseif water_boil_ndxs(i) == 0
		water_boil_ndx = NaN;
	else
		err = MException('BadInput:WaterBoil', 'Water boiling before test start, test %d', i)
		throw(err)
	end
	if lighter_stop_ndxs(i) >= test_start_ndxs(i)
		lighter_stop_ndx = lighter_stop_ndxs(i)-test_start_ndxs(i)+1;
	elseif lighter_stop_ndxs(i) == 0
		lighter_stop_ndx = NaN;
	else
		err = MException('BadInput:LighterStop', 'Lighter stopping before test start, test %d', i)
		throw(err)
	end

	start_datetime = datenum(fgetl(start_dt_fid), 'yyyy-mm-dd HH:MM:SS');
	result = struct('start_datetime', start_datetime, 'water_boil_ndx', water_boil_ndx , 'lighter_stop_ndx', lighter_stop_ndx, 'fuel_consumed_g', fuel_consumed_g(i), 'toxin_data', toxin_data, 'fuel_type', fuel_type);
	stove_results = [stove_results, result];
end

save('results_data/stove_results.mat', 'stove_results')