function [box_data, grouping, colors] = update_box_data(box_data, grouping, colors, new_data, color_map)
	if isempty(grouping)
		i = 1;
	else
		i = max(grouping) + 1;
	end
	for fuel = {'WC', 'AWC'}
		new_color = color_map.(fuel{:});
		box_data = horzcat(box_data, new_data.(fuel{:}));
		grouping = horzcat(grouping, i * ones(1, length(new_data.(fuel{:}))));
		colors = horzcat(colors, new_color);
		i = i + 1;
	end
end